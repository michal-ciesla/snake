import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame window = new JFrame("snake");
        window.setContentPane(new GamePanel());
        window.pack();
        window.setDefaultCloseOperation(window.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.pack();
        //window.setPreferredSize(new Dimension(GamePanel.WIDTH, GamePanel.HEIGHT));
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
}
