import com.sun.glass.ui.Size;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements KeyListener, Runnable {

    public static final int WIDTH = 400;
    public static final int HEIGHT = 400;

    //Render
    private Graphics2D graphics2D;
    private BufferedImage image;

    //Game loop
    private Thread thread;
    private boolean play;
    private long targetTime;

    //Game Stuff
    private static final int SIZE = 10;
    private Entity head;
    private Entity apple;
    private List<Entity> snake;
    private int score;
    private int level;
    private boolean gameOver;

    //movement
    private int dx;
    private int dy;

    //key input
    private boolean up;
    private boolean down;
    private boolean right;
    private boolean left;
    private boolean start;

    public GamePanel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setFocusable(true);
        requestFocus();
        addKeyListener(this);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        thread = new Thread(this);
        thread.start();
    }

    private void setFPS(int fps) {
        targetTime = 1000 / fps;
    }

    public void keyTyped(KeyEvent e) {}

    public void keyPressed(KeyEvent e) {
        int k = e.getKeyCode();
        if (k == KeyEvent.VK_UP) up = true;
        if (k == KeyEvent.VK_DOWN) down = true;
        if (k == KeyEvent.VK_LEFT) left = true;
        if (k == KeyEvent.VK_RIGHT) right = true;
        if (k == KeyEvent.VK_ENTER) start = true;
    }

    public void keyReleased(KeyEvent e) {
        int k = e.getKeyCode();
        if (k == KeyEvent.VK_UP) up = false;
        if (k == KeyEvent.VK_DOWN) down = false;
        if (k == KeyEvent.VK_LEFT) left = false;
        if (k == KeyEvent.VK_RIGHT) right = false;
        if (k == KeyEvent.VK_ENTER) start = false;
    }

    public void run() {
        if (play) return;
        init();
        long startTime;
        long elapsed;
        long wait;

        while (play) {
            startTime = System.nanoTime();
            update();
            requestRender();
            elapsed = System.nanoTime() - startTime;
            wait = targetTime - elapsed / 1000000;

            if (wait > 0) {
                try {
                    Thread.sleep(wait);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setUpLevel() {
        snake = new ArrayList<Entity>();
        head = new Entity(SIZE);
        head.setPosition(WIDTH / 2, HEIGHT / 2);
        snake.add(head);
        for (int i = 1; i < 3; i++) {
            Entity body = new Entity(SIZE);
            body.setPosition(head.getX() + (i * SIZE), head.getY());
            snake.add(body);
        }

        apple = new Entity(SIZE);
        setApple();
        score = 0;
        gameOver = false;
        level = 1;
        dx = dy = 0;
        setFPS(level*10);
    }

    public void setApple() {
        int x = (int)(Math.random() * (WIDTH -SIZE));
        int y = (int)(Math.random() * (HEIGHT-SIZE));
        y= y-(y%SIZE);
        x = x - (x% SIZE);
        apple.setPosition(x,y);
    }

    private void requestRender() {
        render(graphics2D);
        Graphics graphics = getGraphics();
        graphics.drawImage(image, 0, 0, null);
        graphics.dispose();
    }

    private void update() {
        if(gameOver) {
            if (start) {
                setUpLevel();
            }
            return;
        }
        if (up && dy == 0) {
            dy = -SIZE;
            dx = 0;
        } else if (down && dy == 0) {
            dy = SIZE;
            dx = 0;
        } else if (left && dx == 0) {
            dy = 0;
            dx = -SIZE;
        } else if (right && dx == 0 && dy!=0) {
            dy = 0;
            dx = SIZE;
        }
        if (dx != 0 || dy != 0) {
            for (int i = snake.size() - 1; i > 0; i--) {
                snake.get(i).setPosition(
                        snake.get(i - 1).getX(),
                        snake.get(i - 1).getY()
                );
            }
            head.move(dx, dy);
        }

        for(Entity entity: snake) {
            if(entity.isCollision(head)) {
                gameOver = true;
                break;
            }
        }

        if(apple.isCollision(head)) {
            score++;
            setApple();
            Entity body = new Entity(SIZE);
            body.setPosition(-20, -20);
            snake.add(body);
            if(score %10 == 0) {
                level++;
                setFPS(level*10);
            }
        }
        if (head.getX() < 0) head.setX(WIDTH);
        else if (head.getY() < 0) head.setY(HEIGHT);
        else if (head.getX() > WIDTH) head.setX(0);
        else if (head.getY() > HEIGHT) head.setY(0);
    }

    private void init() {
        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        graphics2D = image.createGraphics();
        play = true;
        setUpLevel();

    }

    public void render(Graphics2D graphics2D) {
        graphics2D.clearRect(0, 0, WIDTH, HEIGHT);
        graphics2D.setColor(Color.GREEN);
        for (Entity entity : snake) {
            entity.render(graphics2D);
        }

        graphics2D.setColor(Color.RED);
        apple.render(graphics2D);
        if(gameOver) {
            graphics2D.drawString( "Game Over!", 150, 200);
        }

        graphics2D.setColor(Color.WHITE);
        graphics2D.drawString("Score: " + score + " Level: " + level, 10,10);
        if(dx == 0 && dy ==0) {
            graphics2D.drawString( "Ready?", 200, 200);
        }
    }
}
